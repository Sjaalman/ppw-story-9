from django.contrib import admin
from django.urls import path

from . import views

app_name = "story9"

urlpatterns = [
    path('', views.index, name = "index"),
    path('register', views.register, name = "register"),
    path('logout', views.logout_view, name = 'logout'),
    path('nama', views.nama, name = "nama"),
]
