from django.test import TestCase, Client
from django.urls import reverse

# Create your tests here.
class loginFunctionTests(TestCase):
    def test_GET_index(self):
        response = self.client.get(reverse('story9:index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,"index.html")

    def test_GET_register(self):
        response = self.client.get(reverse('story9:register'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,"registration/register.html")

    def test_register_user(self):
        response = self.client.post(reverse("story9:register"), data = {
            "username" : "a",
            "password1" : "samplepass6969",
            "password2" : "samplepass6969"
        }, follow=True)
        # check if valid user registration redirects to homepage
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "a")
        self.assertTemplateUsed(response, "registration/login.html")

        


