from django.shortcuts import render, redirect
from django.contrib.auth import logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required

# Create your views here.
def index(request):
    context = {
        "username" : request.user.username if request.user.is_authenticated else ""
    }
    return render(request, "index.html", context)

@login_required(login_url='/account/login')
def nama(request):
    vocals = ['a', 'i', 'u', 'e', 'o']
    name = request.user.username

    for ch in name.lower():
        if ch in vocals:
            name = name.replace(ch, 'x')

    context = {
        "username" : name
    }
    return render(request, "nama.html", context)

def register(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)

        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            return redirect('account/login')

    else:
        form = UserCreationForm()

    context = {'form' : form}
    return render(request, "registration/register.html", context)

def logout_view(request):
    logout(request)
    return redirect('/')